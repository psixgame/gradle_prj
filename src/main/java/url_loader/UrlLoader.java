package src.url_loader;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static src.Logger.log;

public final class UrlLoader {

	private static final String URL = "https://api.thecatapi.com/v1/images/search";

	public static UrlLoader create(final int count, final String fileName) {
		return new UrlLoader(count, fileName);
	}

	private final int count;
	private final String fileName;

	private UrlLoader(final int count, final String fileName) {
		this.count = count;
		this.fileName = fileName;
	}

	public final void loadUrls() throws IOException {
	    createFile();
	    log(String.format("Loading %1$d urls", count));
	    final List<String> urls = new ArrayList<>(count);
	    for (int i = 0; i < count; i++) {
            final String jsonStr = loadJson(URL);
            final String imgUrl = getUrlFromJson(jsonStr);
            log("Loaded url: " + imgUrl);
            urls.add(imgUrl);
        }
        log("Loading urls finished");
        log(String.format("Writing to %1$s file", fileName));
	    writeToFile(urls);
        log("Successfully wrote to file");
    }

	private String loadJson(final String endpoint) throws IOException {
		final StringBuilder result = new StringBuilder();
		final URL url = new URL(endpoint);
		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
        final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		rd.close();
		return result.toString();
	}

	private String getUrlFromJson(final String jsonStr) {
	    final int https = jsonStr.indexOf("https");
	    final int end = jsonStr.indexOf("\"", https);
	    return jsonStr.substring(https, end);
    }

    private void writeToFile(final List<String> strs) throws IOException {
	    final BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
	    for (final String str : strs) {
	        writer.write(str);
	        writer.newLine();
        }
        writer.close();
    }

    private void createFile() throws IOException {
	    new File(fileName).createNewFile();
    }
	
}