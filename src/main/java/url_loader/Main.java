package src.url_loader;

import src.Logger;

import java.io.IOException;

public final class Main {

    public static void main(final String args[]) throws IOException {
        if (args.length < 2) {
            Logger.log("Set all arguments");
            exit();
        } else {
            final String number = args[0];
            final String name = args[1];
            int count = -1;
            try {
                count = Integer.parseInt(number);
            } catch (final NumberFormatException e) {
                Logger.log("Invalid number: " + number);
                exit();
            }

            UrlLoader.create(count, name).loadUrls();
        }
    }

    private static void exit() {
        System.exit(1);
    }

}
