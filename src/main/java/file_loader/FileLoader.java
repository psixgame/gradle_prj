package src.file_loader;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static src.Logger.log;

public final class FileLoader {

    public static FileLoader create(final String[] urls) {
        return new FileLoader(new ArrayList<>(Arrays.asList(urls)));
    }

    private final List<String> urls;

    private FileLoader(List<String> urls) {
        this.urls = urls;
    }

    public void loadFiles() throws IOException {
        log(String.format("Loading %1$d files", urls.size()));
        for (final String url : urls) {
            loadFile(url);
        }
        log("Loading files finished");
    }

    private void loadFile(final String url) throws IOException {
        final String file = getNameFromUrl(url);

        try (final BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
             final FileOutputStream out = new FileOutputStream(file)) {
            final byte data[] = new byte[4096];
            int c;
            while ((c = in.read(data, 0, 4096)) != -1) {
                out.write(data, 0, c);
            }
        }

        log("Loaded file: " + file);
    }

    private String getNameFromUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

}
