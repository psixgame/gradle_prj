package src.file_loader;

import src.Logger;

import java.io.IOException;

public class Main {

    public static void main(final String args[]) throws IOException {
        if (args.length < 1) {
            Logger.log("Provide arguments");
            exit();
        } else {
            FileLoader.create(args).loadFiles();
        }
    }

    private static void exit() {
        System.exit(1);
    }
}
